# Go search query always returns 0 documents

Using the [**Search API**][0] in Go and the App Engine standard environment, documents `Put` into an index are not retrieved when searching the index in which they were put.

## Steps to reproduce

* Download this reproduction and go to its directory

```shell
git clone git@bitbucket.org:cpstelus/gae-go-search.git
cd gae-go-search
```

* Run the reproduction in development

```shell
dev_appserver.py app.yaml
```

* Populate the index with a few documents by going to `localhost:8080/add`

* List the documents in the index by going to `localhost:8080/list`

* Search the index by going to `localhost:8080/search`.  No documents are listed regardless of time or search criteria.

[0]: https://cloud.google.com/appengine/docs/go/search/
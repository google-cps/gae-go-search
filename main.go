package godo

import (
	"fmt"
	"net/http"
	"google.golang.org/appengine"
	"google.golang.org/appengine/search"
)

func init() {
	http.HandleFunc("/add", handle_add)
	http.HandleFunc("/search", handle_search)
	http.HandleFunc("/list", handle_list)
}

type Example struct {
	Name string
}

var index_name string = "collection"

func handle_add(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	index, index_open_err := search.Open(index_name)
	if index_open_err != nil {
		fmt.Fprintf(w, "Could not open index '%s'\n%+v\n", index_name, index_open_err)
		return
	}
	fmt.Fprintf(w, "Openeed index '%s'\n", index_name)

	example_to_commit := &Example{Name: "aardvark"}

	new_id, commit_err := index.Put(ctx, "", example_to_commit)
	if commit_err != nil {
		fmt.Fprintf(w, "Could not commit example %+v\n%+v\n", example_to_commit, commit_err)
		return
	}
	fmt.Fprintf(w, "Comitted example %+v with id %s\n", example_to_commit, new_id)
}

func handle_search(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	name := "aardvark"
	query := fmt.Sprintf("Name: %s", name)

	index, index_open_err := search.Open(index_name)
	if index_open_err != nil {
		fmt.Fprintf(w, "Could not open index '%s'\n%+v\n", index_name, index_open_err)
		return
	}
	fmt.Fprintf(w, "Opened index '%s'\n", index_name)

	iterator := index.Search(ctx, query, nil)
	fmt.Fprintf(w, "Searched index '%s' with query '%s'\nQuery returned %d documents\n", index_name, query, iterator.Count())
}

func handle_list(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)

	index, index_open_err := search.Open(index_name)
	if index_open_err != nil {
		fmt.Fprintf(w, "Could not open index '%s'\n%+v\n", index_name, index_open_err)
		return
	}
	fmt.Fprintf(w, "Opened index '%s'\n", index_name)

	iterator := index.List(ctx, nil)
	fmt.Fprintf(w, "Listing all documents in index '%s'\n", iterator.Count(), index_name)
	for true {
		var doc Example
		id, next_err := iterator.Next(&doc)
		if next_err == search.Done {
			break
		}
		if next_err != nil {
			fmt.Fprintf(w, "Error searching index '%s'\n%+v\n", index_name, next_err)
			break
		}
		fmt.Fprintf(w, "%s => %+v\n", id, doc)
	}
}
